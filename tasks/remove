#!/usr/bin/env node

var fs = require('fs'),
    path = require('path'),
    util = require('util');

var args = process.argv,
    basedir = path.join(path.dirname(args[1]), '../web'),
    modname = args[2];

if (!modname) {
    console.error('Usage: remove <module_name>');
    process.exit(1);
}

var rmdir = function (dir) {
    if (fs.existsSync(dir)) {
        fs.readdirSync(dir).forEach(function (file, index) {
            var curPath = path.join(dir, file);
            if (fs.statSync(curPath).isDirectory())
                rmdir(curPath);
            else
                fs.unlinkSync(curPath);
        });
        fs.rmdirSync(dir);
    }
};

// remove scripts directory
(function removeScripts () {
    rmdir(path.join(basedir, 'scripts/modules', modname));
    // remove module from scripts/modules/all.js
    var mainjs = path.join(basedir, 'scripts/modules/all.js');
    fs.readFile(mainjs, function (err, data) {
        if (err !== null) {
            console.log(err);
            return;
        }
        var modjs = util.format('    Modules.%s = {[^]*};.*?\n\n', modname.charAt(0).toUpperCase() + modname.slice(1)),
            contents = data.toString().replace(new RegExp(modjs, 'g'), '');
        fs.writeFile(mainjs, contents);
    });
})();

// remove static directory
(function createStatic () {
    rmdir(path.join(basedir, 'static/images/modules', modname));
})();

// remove style.scss
(function createStyles () {
    fs.unlinkSync(path.join(basedir, 'styles/modules', modname + '.scss'));
    // remove module from styles/main.scss
    var mainscss = path.join(basedir, 'styles/main.scss');
    fs.readFile(mainscss, function (err, data) {
        if (err !== null) {
            console.log(err);
            return;
        }
        var modscss = util.format('\n@import "modules/%s";', modname),
            contents = data.toString().replace(new RegExp(modscss, 'g'), '');
        fs.writeFile(mainscss, contents);
    });
})();

// create template directory
(function createTemplates () {
    rmdir(path.join(basedir, 'templates', modname));
})();
