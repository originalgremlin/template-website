(function () {
	var debug = window.location.host === 'localhost';

	require.config({
		config: {
			'modules/main/models': {
				urlRoot: '/api/egg-energy'
			}
		},
		paths: {
			async: 'lib/requirejs-plugins/async',
			backbone: 'lib/backbone.custom',
			backbone_rollup: 'lib/backbone.rollup',
			gmaps: 'lib/requirejs-plugins/gmaps',
			i18n: 'lib/requirejs-plugins/i18n',
			jquery: 'lib/jquery',
			templates: '../templates',
			text: 'lib/requirejs-plugins/text',
			underscore: 'lib/underscore.custom'
		},
		shim: {
			'lib/backbone': { deps: ['underscore', 'jquery'], exports: 'Backbone' },
			'lib/backbone.collectionbinder': { deps: ['backbone', 'lib/backbone.modelbinder'] },
			'lib/backbone.validation': { deps: ['backbone'] },
			'lib/d3.v2': { exports: 'd3' },
			'lib/jquery.ui': { deps: ['jquery'] },
			'lib/json2': { exports: 'JSON' },
			'lib/rickshaw': { deps: ['lib/d3.v2'], exports: 'Rickshaw' },
			'lib/underscore': { exports: '_' },
			'lib/underscore.custom': { deps: ['lib/underscore'], exports: '_' },
			'lib/uuid': { exports: 'uuid' }
		},
		urlArgs: debug ? (new Date()).getTime() : undefined,
		waitSeconds: 60
	});

	requirejs.onError = function (error) {
		console.log(error);
	};

	define(function (require) {
		require('modules/controller');
	});
})();
