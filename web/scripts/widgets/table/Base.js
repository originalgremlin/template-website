﻿define(function (require) {
    'use strict';

    var _ = require('underscore'),
        $ = require('jquery'),
        Backbone = require('backbone_rollup'),
        Widgets = require('widgets/Base'),
        Table = { },
        i18n = require('lib/i18n');

    Table.Row = Widgets.ItemView.extend({
        tagName: 'tr',
        widgetClassName: 'row',
        fields: null,

        initialize: function (options) {
            this._super('initialize', arguments);
            // define display fields
            this.fields = this.fields || this.parent.fields || _.keys(this.model.toJSON());
            this.selected = false;
            // create row template
            if (_.isUndefined(this.template))
                this.template = this._getTemplate();
            // enable model binding
            this.binding = new Backbone.ModelBinder();
        },

        _getTemplate: function () {
            var template = '';
            for (var i = 0, length = this.fields.length; i < length; i++)
                template += _.string.sprintf('<td name="%s"></td>', this.fields[i]);
            return template;
        },

        onRender: function () {
            this._super('onRender', arguments);
            this.binding.bind(this.model, this.$el);
        },

        onClose: function () {
            this._super('onClose', arguments);
            this.binding.unbind();
        }
    });

    // displayed when no data is available to render
    Table.EmptyRow = Widgets.ItemView.extend({
        tagName: 'tr',
        widgetClassName: 'emptyrow',
        template: '<td>' + i18n.gettext('No data available.') + '</td>',

        onRender: function () {
            // fix the colspan
            this.$('td').attr('colspan', this.parent.headings.length);
        }
    });

    Table.Base = Widgets.CompositeView.extend({
        tagName: 'table',
        widgetClassName: 'table',
        // views
        itemViewContainer: 'tbody',
        itemView: Table.Row,
        emptyView: Table.EmptyRow,
        // column labels
        fields: null,
        headings: null,

        widgetEvents: {
            'click th.sortable': 'onHeaderClick'
        },

        initialize: function (options) {
            _.bindAll(this, 'onScroll');
            this._super('initialize', arguments);
            // define display fields
            if (_.isNull(this.fields)) {
                var model = this.model || this.collection.first();
                this.fields = model ? _.keys(model.toJSON()) : [];
            }
            this.fields = _.map(this.fields, function (f) { return i18n.gettext(f); });
            // define display headings
            if (_.isNull(this.headings)) {
                this.headings = _.map(this.fields, function (field) {
                    return _.string.titleize(_.string.humanize(field));
                });
            }
            this.headings = _.map(this.headings, function (h) { return i18n.gettext(h); });
            // create table template and edit dialog
            if (_.isUndefined(this.template))
                this.template = this._getTemplate();
        },

        _getTemplate: function () {
            return _.string.sprintf(
                '<thead>%s</thead><tbody>%s</tbody><tfoot>%s</tfoot>',
                this._getHead(), this._getBody(), this._getFoot()
            );
        },

        _getHead: function () {
            var head = '';
            for (var i = 0, length = this.fields.length; i < length; i++)
                head += _.string.sprintf('<th class="sortable %s">%s</th>', this.fields[i], this.headings[i]);
            return '<tr>' + head + '</tr>';
        },

        _getBody: function () {
            return '';
        },

        _getFoot: function () {
            return '';
        },

        sort: function (field, descending) {
            // set defaults
            if (_.isUndefined(field))
                field = this.fields[0];
            if (_.isUndefined(descending))
                descending = (this.collection.urlOptions.view === field) ? !this.collection.urlOptions.descending : false;
            // update the dom
            this.$('thead th').removeClass('ascending descending');
            this.$('thead th.' + field).addClass(descending ? 'descending' : 'ascending');
            // fetch the collection
            this.collection.urlOptions.descending = descending;
            this.collection.urlOptions.view = field;
            this.collection.urlOptions.startkey = undefined;
            this.collection.urlOptions.startkey_docid = undefined;
            this.collection.fetch();
        },

        onShow: function () {
            this._super('onShow', arguments);
            this.collection.fetch();
            this.$el.parent().on('scroll', this.onScroll);
        },

        beforeClose: function () {
            this._super('beforeClose', arguments);
            this.$el.parent().off('scroll', this.onScroll);
        },

        onHeaderClick: function (event) {
            var target = $(event.target),
                field = this.fields[target.index()];
            this.sort(field);
        },

        onScroll: function (event) {
            if (this.collection.fetching)
                return;

            var length = this.collection.length;
            if (length <= 0 || length >= this.collection.total_rows)
                return;

            var target = event.target,
                scrollBottom = target.scrollTop + target.offsetHeight,
                scrollHeight = target.scrollHeight,
                rowHeight = this.$('tbody tr:first').height();
            if (scrollBottom >= scrollHeight - rowHeight * 25) {
                var last = this.collection.last();
                this.collection.urlOptions.startkey = last.get(this.collection.urlOptions.view) || '';
                this.collection.urlOptions.startkey_docid = last.get('_id');
                this.collection.fetch({ add: true, remove: false, update: true });
            }
        }
    });

    return Table;
});
