﻿define(function (require) {
    'use strict';

    var _ = require('underscore'),
        $ = require('jquery'),
        Backbone = require('backbone_rollup'),
        Widgets = require('widgets/Base'),
        Mixins = { };

    // a convenience method to allow inclusion of mixins in backbone, marionette, and widgets
    // uses jQuery's deep-copy extend method
    // example: var x = Widgets.Mixin(Mixins.Visible, {...});
    var mixin = function () {
        var args = Array.prototype.slice.call(arguments);
        args.unshift(true, {});
        return this.extend($.extend.apply(this, args));
    };
    _.each(["Model", "Collection", "View", "Router"], function (klass) {
        Backbone[klass].mixin = mixin;
    });
    _.each(["ItemView", "CollectionView", "CompositeView", "Layout"], function (klass) {
        Backbone.Marionette[klass].mixin = Widgets[klass].mixin = mixin;
    });

    Mixins.Alignments = {
        CENTER: 0,
        TOP_LEFT: 1,
        TOP_MIDDLE: 2,
        TOP_RIGHT: 3,
        BOTTOM_LEFT: 4,
        BOTTOM_MIDDLE: 5,
        BOTTOM_RIGHT: 6,
        MIDDLE_RIGHT: 7,
        MIDDLE_LEFT: 8,
        OUTSIDE_TOP_LEFT: 9,
        OUTSIDE_TOP_MIDDLE: 10,
        OUTSIDE_TOP_RIGHT: 11,
        OUTSIDE_BOTTOM_LEFT: 12,
        OUTSIDE_BOTTOM_MIDDLE: 13,
        OUTSIDE_BOTTOM_RIGHT: 14,
        OUTSIDE_RIGHT_TOP: 15,
        OUTSIDE_RIGHT_MIDDLE: 16,
        OUTSIDE_RIGHT_BOTTOM: 17,
        OUTSIDE_LEFT_TOP: 18,
        OUTSIDE_LEFT_MIDDLE: 19,
        OUTSIDE_LEFT_BOTTOM: 20
    };


    Mixins.Visible = {
        show: function () {
            this.$el.show();
        },

        hide: function () {
            this.$el.hide();
        },

        appear: function (duration, easing, callback) {
            var self = this;
            this.$el.fadeIn(duration, easing, function () {
                if (callback)
                    callback();
                if (self.onAppear)
                    self.onAppear();
            });
        },

        disappear: function (duration, easing, callback) {
            var self = this;
            this.$el.fadeOut(duration, easing, function () {
                if (callback)
                    callback();
                if (self.onDisappear)
                    self.onDisappear();
            });
        }
    };


    Mixins.Positionable = {
        position: function (el, alignment) {
            // cache for use with reposition
            this._mixins_position_container = $(el);
            this._mixins_position_alignment = alignment;
            var containerElPos = this._mixins_position_container.offset(),
                container = {
                    height: this._mixins_position_container.height(),
                    width: this._mixins_position_container.width(),
                    top: containerElPos.top,
                    left: containerElPos.left
                },
                control = {
                    height: this.$el.height(),
                    width: this.$el.width(),
                    top: undefined,
                    left: undefined
                };

            switch (alignment) {
                case Mixins.Alignments.TOP_LEFT:
                    control.top = container.top;
                    control.left = container.left;
                    break;
                case Mixins.Alignments.TOP_MIDDLE:
                    control.top = container.top;
                    control.left = container.left + (container.width - control.width) / 2;
                    break;
                case Mixins.Alignments.TOP_RIGHT:
                    control.top = container.top;
                    control.left = container.left + container.width - control.width;
                    break;
                case Mixins.Alignments.BOTTOM_LEFT:
                    control.top = container.top + container.height - control.height;
                    control.left = container.left;
                    break;
                case Mixins.Alignments.BOTTOM_MIDDLE:
                    control.top = container.top + container.height - control.height;
                    control.left = container.left + (container.width - control.width) / 2;
                    break;
                case Mixins.Alignments.BOTTOM_RIGHT:
                    control.top = container.top + container.height - control.height;
                    control.left = container.left + container.width - control.width;
                    break;
                case Mixins.Alignments.MIDDLE_RIGHT:
                    control.top = container.top + (container.height - control.height) / 2;
                    control.left = container.left + container.width - control.width;
                    break;
                case Mixins.Alignments.MIDDLE_LEFT:
                    control.top = container.top + (container.height - control.height) / 2;
                    control.left = container.left;
                    break;
                case Mixins.Alignments.OUTSIDE_TOP_LEFT:
                    control.top = container.top - control.height;
                    control.left = container.left;
                    break;
                case Mixins.Alignments.OUTSIDE_TOP_MIDDLE:
                    control.top = container.top - control.height;
                    control.left = container.left + (container.width - control.width) / 2;
                    break;
                case Mixins.Alignments.OUTSIDE_TOP_RIGHT:
                    control.top = container.top - control.height;
                    control.left = container.left + container.width - control.width;
                    break;
                case Mixins.Alignments.OUTSIDE_BOTTOM_LEFT:
                    control.top = container.top + container.height;
                    control.left = container.left;
                    break;
                case Mixins.Alignments.OUTSIDE_BOTTOM_MIDDLE:
                    control.top = container.top + container.height;
                    control.left = container.left + (container.width - control.width) / 2;
                    break;
                case Mixins.Alignments.OUTSIDE_BOTTOM_RIGHT:
                    control.top = container.top + container.height;
                    control.left = container.left + container.width - control.width;
                    break;
                case Mixins.Alignments.OUTSIDE_RIGHT_TOP:
                    control.top = container.top;
                    control.left = container.left + container.width;
                    break;
                case Mixins.Alignments.OUTSIDE_RIGHT_MIDDLE:
                    control.top = container.top + (container.height - control.height) / 2;
                    control.left = container.left + container.width;
                    break;
                case Mixins.Alignments.OUTSIDE_RIGHT_BOTTOM:
                    control.top = container.top + container.height - control.height;
                    control.left = container.left + container.width;
                    break;
                case Mixins.Alignments.OUTSIDE_LEFT_TOP:
                    control.top = container.top;
                    control.left = container.left - control.width;
                    break;
                case Mixins.Alignments.OUTSIDE_LEFT_MIDDLE:
                    control.top = container.top + (container.height - control.height) / 2;
                    control.left = container.left - control.width;
                    break;
                case Mixins.Alignments.OUTSIDE_LEFT_BOTTOM:
                    control.top = container.top + container.height - control.height;
                    control.left = container.left - control.width;
                    break;
                default:
                case Mixins.Alignments.CENTER:
                    control.top = container.top + (container.height - control.height) / 2;
                    control.left = container.left + (container.width - control.width) / 2;
                    break;
            }

            if (!_.isUndefined(control.top) && !_.isUndefined(control.left)) {
                this.$el.css({
                    position: this.options.fixed === false ? 'absolute' : 'fixed',
                    left: control.left,
                    top: control.top
                });
            }
        },

        reposition: function () {
            this.position(this._mixins_position_container || this.container, this._mixins_position_alignment);
        }
    };

    return Mixins;
});
