﻿define(function (require) {
    'use strict';

    var _ = require('underscore'),
        Backbone = require('backbone_rollup');

    var Widget = {
        tagName: 'div',

        // The events function searches the inherited class tree for properties called 'widgetEvents' and
        // collects them into a single container. This function is used by BackBone.
        events: function () {
            var rv = {}, parent = this;
            do {
                if (parent.widgetEvents)
                    _.extend(rv, _.result(parent, 'widgetEvents') || {});
            } while (parent = parent.constructor.__super__);
            return rv;
        },

        // The className function walks the inheritence tree collecting properties called 'widgetClassName'
        // and concatenating them into a single string. This function is used by BackBone to attach
        // classnames to an object.
        className: function () {
            var rv = [], parent = this;
            do {
                if (parent.widgetClassName)
                    rv.unshift(parent.widgetClassName);
            } while (parent = parent.constructor.__super__);
            return _.uniq(rv).join(' ');
        },

        // Used by the className property to attach classes to an object. Properties specified here
        // are appended to properties the inheritence tree. If you wish to override all classNames,
        // just override the className property.
        widgetClassName: 'widget',

        // Used by the events property to attach events to an object. Properties specified here
        // are appended to properties the inheritence tree. If you wish to override all events,
        // just override the event property.
        widgetEvents: {},

        // lets subclasses can call this._super('onRender', arguments) without error
        // currently all no-ops
        beforeClose: function () { },
        beforeRender: function () { },
        onRender: function () { },
        onShow: function () { },
        onClose: function () { }
    };

    var Widgets = { };
    _.each(["ItemView", "CollectionView", "CompositeView", "Layout"], function (klass) {
        Widgets[klass] = Backbone.Marionette[klass].extend(Widget);
    });

    return Widgets;
});