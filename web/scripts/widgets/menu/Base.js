/*
    var m = new Widgets.Menu.Base({
        header: '#header',
        content: '#content',
        buttons: [{
            name: 'button 1',
            trigger: {
                vent: App.vent,
                name: 'do:something',
                data: [1,2,3, 'triggering an event']
            }
        }, {
            name: 'button 2',
            callback: function () { console.log('using a callback'); }
        }],
        menus: [{
            name: 'a menu with items',
            items: [{
                name: 'item 1',
                view: Backbone.Marionette.ItemView,
                options: { template: 'item view 1' }
            }, {
                name: 'item 2',
                view: Backbone.Marionette.ItemView,
                options: { template: 'item view 2' }
            }]
        }, {
            name: 'this menu has no items',
            view: Backbone.Marionette.ItemView,
            options: { template: 'item view 3' }
        }]
    });
    m.render();
    $('body').prepend(m.el);
*/

define(function (require) {
    'use strict';

    var _ = require('underscore'),
        $ = require('jquery'),
        Backbone = require('backbone_rollup'),
        Widgets = require('widgets/Base'),
        Menu = { },
        i18n = require('lib/i18n'),
        gettext = i18n.gettext;

    Menu.Base = Widgets.Layout.extend({
        tagName: 'ul',
        widgetClassName: 'menu',
        widgetEvents: {
            'click li.topitem > a': 'onTopItemClick'
        },

        initialize: function (options) {
            // check for defaults
            options = options || {};
            var msg, err;
            if (!options.content || $(options.content).length === 0) {
                msg = "Could not find content region: '" + options.content + "'";
                err = new Error(msg);
                err.name = "NoContentRegionError";
                throw err;
            }
            // initialize
            this._super('initialize', arguments);
            this.parent = options.parent || null;
        },

        onRender: function () {
            // buttons
            _.each(this.options.buttons, function (button) {
                var menuButton = new Menu.Button({
                    name: button.name,
                    callback: button.callback,
                    trigger: button.trigger
                });
                menuButton.render();
                this.$el.append(menuButton.el);
            }, this);

            // items
            var selected;
            _.each(this.options.items, function (item) {
                // branch: create submenu control
                if (item.items) {
                    var subMenu = new Menu.Base({
                        content: this.options.content,
                        parent: this,
                        buttons: item.buttons,
                        items: item.items
                    });
                    subMenu.render();
                    this.$el.append($('<li class="topitem"><a>' + item.name + '</a></li>').append(subMenu.el));
                // leaf: create item view
                } else if (item.view) {
                    var menuItem = new Menu.Item({
                        menu: this,
                        name: item.name,
                        view: item.view,
                        options: item.options
                    });
                    menuItem.render();
                    if (item.selected)
                        selected = menuItem;
                    this.$el.append(menuItem.el);
                }
            }, this);

            // display items and content
            $(this.options.header).append(this.el);
            this.regionManagers.content = this.content = new Backbone.Marionette.Region({
                el: this.options.content
            });

            // select at most one menu item
            if (selected)
                this.onItemClick(selected);
        },

        onTopItemClick: function (event) {
            var target = $(event.currentTarget),
                menuitem = target.next().children('li.menuitem').first();
            menuitem.click();
        },

        onItemClick: function (item) {
            // clear existing selections
            var parents = [],
                parent = this;
            while (parent) {
                parents.push(parent);
                parent = parent.parent;
            }
            parents.pop().$('.selected').removeClass('selected');
            // add new selections
            item.$el.addClass('selected');
            _.each(parents, function (parent) {
                parent.$el.parent('li').addClass('selected');
            });
            // display the selected view
            this.content.show(new item.options.view(item.options.options));
        }
    });

    Menu.Button = Widgets.ItemView.extend({
        tagName: 'li',
        widgetClassName: 'menubutton',
        widgetEvents: {
            'click': 'onClick'
        },

        onRender: function () {
            this.$el.html('<a>' + this.options.name + '</a>');
        },

        onClick: function (event) {
            var callback = this.options.callback,
                triggerOption = this.options.trigger;
            if (callback)
                callback();
            if (triggerOption && triggerOption.vent && triggerOption.name)
                triggerOption.vent.trigger(triggerOption.name, triggerOption.data);
            event.stopPropagation();
        }
    });

    Menu.Item = Widgets.ItemView.extend({
        tagName: 'li',
        widgetClassName: 'menuitem',
        widgetEvents: {
            'click': 'onClick'
        },

        onRender: function () {
            this.$el.html('<a>' + this.options.name + '</a>');
        },

        onClick: function (event) {
            if (!this.$el.hasClass('selected'))
                this.options.menu.onItemClick(this);
        }
    });

    return Menu;
});
