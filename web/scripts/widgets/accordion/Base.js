﻿/*
    var a = new Widgets.Accordion.Base({
        content: '#content',
        tabs: [
            {name: 'one', view: Backbone.Marionette.ItemView, options: {template: 'one content'}},
            {name: 'two', view: Backbone.Marionette.ItemView, options: {template: 'two content'}},
            {name: 'three', view: Backbone.Marionette.ItemView, options: {template: 'three content'}}
        ]
    });
    a.render();
    $('body').prepend(a.el);
*/

define(function (require) {
    'use strict';

    var _ = require('underscore'),
        $ = require('jquery'),
        Backbone = require('backbone_rollup'),
        Widgets = require('widgets/Base'),
        Accordion = { },
        i18n = require('lib/i18n'),
        gettext = i18n.gettext;

    Accordion.Base = Widgets.Layout.extend({
        tagName: 'dl',
        widgetClassName: 'accordion',
        widgetEvents: {
            'click dt': 'onTabClick'
        },

        initialize: function (options) {
            this._super('initialize', arguments);
            this.tabs = this.options.tabs;
            this.multiple = this.options.multiple || false;
        },

        addTab: function (tab) {
            var dt = $('<dt>' + tab.name + '</dt>'),
                dd = $('<dd />').append((new tab.view(tab.options)).render().$el).hide();
            this.$el.append(dt, dd);
        },

        onRender: function () {
            _.each(this.tabs, function (tab) {
                this.addTab(tab);
            }, this);
        },

        onTabClick: function (event) {
            var index = $(event.target).index('dt');
            this.select(index);
        },

        select: function (index) {
            // set tab styling
            var dts = this.$('dt'),
                dds = this.$('dd'),
                dt = dts.eq(index),
                dd = dds.eq(index);
            // show/hide the selected tab
            dt.toggleClass('selected');
            dd.toggleClass('selected').slideToggle();
            // conditionally hide the others
            if (!this.multiple) {
                dts.not(dt).removeClass('selected');
                dds.not(dd).removeClass('selected').slideUp();
            }
            // trigger event
            this.trigger('accordion:selected', index);
        }
    });

    return Accordion;
});
