﻿/*
    var t = new Widgets.Tab.Base({
        header: '#header',
        content: '#content',
        tabs: [
            {name: 'one', view: Backbone.Marionette.ItemView, options: {template: 'one content'}},
            {name: 'two', view: Backbone.Marionette.ItemView, options: {template: 'two content'}},
            {name: 'three', view: Backbone.Marionette.ItemView, options: {template: 'three content'}}
        ]
    });
    t.render();
    $('body').prepend(t.el);
*/

define(function (require) {
    'use strict';

    var _ = require('underscore'),
        $ = require('jquery'),
        Backbone = require('backbone_rollup'),
        Widgets = require('widgets/Base'),
        Tab = { },
        i18n = require('lib/i18n'),
        gettext = i18n.gettext;

    Tab.Base = Widgets.Layout.extend({
        tagName: 'ul',
        widgetClassName: 'tab',
        widgetEvents: {
            'click li:not(.selected)': 'onTabClick'
        },

        initialize: function (options) {
            options = options || {};
            var msg, err;
            if (!options.content || $(options.content).length === 0) {
                msg = "Could not find content region: '" + options.content + "'";
                err = new Error(msg);
                err.name = "NoContentRegionError";
                throw err;
            }
            if (!options.header || $(options.header).length === 0) {
                msg = "Could not find header region: '" + options.header + "'";
                err = new Error(msg);
                err.name = "NoHeaderRegionError";
                throw err;
            }
            this._super('initialize', arguments);
            this.tabs = this.options.tabs;
        },

        onRender: function () {
            // prepare the tabs
            _.each(this.tabs, function (tab) {
                this.$el.append('<li>' + tab.name + '</li>');
            }, this);
            // display tabs and content
            $(this.options.header).html(this.el);
            this.regionManagers.content = this.content = new Backbone.Marionette.Region({
                el: this.options.content
            });
            // possibly select an initial tab
            if (_.isUndefined(this.options.selected))
                this.select(0);
            else if (_.isFinite(this.options.selected))
                this.select(this.options.selected);
        },

        onTabClick: function (event) {
            var index = $(event.target).index();
            this.select(index);
        },

        select: function (index) {
            // set tab styling
            var lis = this.$('li');
            lis.removeClass('selected');
            lis.eq(index).addClass('selected');
            // swap in proper content view to content region
            var tab = this.tabs[index];
            this.content.show(new tab.view(tab.options));
            // trigger event
            this.trigger('tab:selected', index);
        }
    });

    return Tab;
});