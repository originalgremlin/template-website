define(function (require) {
    'use strict';
    require('lib/jquery.ui');

    var _ = require('underscore'),
        $ = require('jquery'),
        Backbone = require('backbone_rollup'),
        Widgets = require('widgets/Base'),
        Chart = { },
        Rickshaw = require('lib/rickshaw'),
        i18n = require('lib/i18n'),
        gettext = i18n.gettext;

    Chart.MONTHS = _.map(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'], gettext);

    Chart.Base = Widgets.ItemView.extend({
        widgetClassName: 'chart',
        renderer: 'bar',
        template: '<header></header><section><div class="y_axis" /><div class="graph" /><div class="slider" /><div class="legend" /></section>',

        initialize: function (options) {
            this._super('initialize', arguments);
            _.bindAll(this, 'renderGraph');
        },

        onRender: function () {
            this.$('header').text(this.title);
            $.get(this.url).then(this.renderGraph);
        },

        renderGraph: function (response) {
            var series = this.parse(JSON.parse(response));
            this.graph = new Rickshaw.Graph({
                element: this.$('.graph')[0],
                renderer: this.renderer,
                series: series,
                stroke: true
            });
            this.renderLegend();
            this.renderXAxis();
            this.renderYAxis();
            this.renderRangeSlider();
            this.renderHoverDetail();
            this.graph.render();
        },

        parse: function (response) {
            return response.rows;
        },

        renderLegend: function () {
            if (this.legend === false)
                return;
            if (this.graph.series.length <= 1)
                return;
            new Rickshaw.Graph.Legend({
                element: this.$('.legend')[0],
                graph: this.graph
            });
        },

        renderXAxis: function () {
            if (this.x_axis === false)
                return;
            new Rickshaw.Graph.Axis.Time({
                graph: this.graph
            });
        },

        renderYAxis: function () {
            if (this.y_axis === false)
                return;
            new Rickshaw.Graph.Axis.Y({
                element: this.$('.y_axis')[0],
                graph: this.graph,
                orientation: 'left',
                tickFormat: Rickshaw.Fixtures.Number.formatKMBT
            });
        },

        renderRangeSlider: function () {
            if (this.rangeSlider === false)
                return;
            new Rickshaw.Graph.RangeSlider({
                element: this.$('.slider'),
                graph: this.graph
            });
        },

        renderHoverDetail: function () {
            if (this.hoverDetail === false)
                return;
            var self = this;
            new Rickshaw.Graph.HoverDetail({
                graph: this.graph,
                xFormatter: function (x) {
                    return x + (self.x_label ? ' ' + self.x_label : '');
                },
                formatter: function (series, x, y) {
                    return y + (self.y_label ? ' ' + self.y_label : '');
                }
            });
        }
    });

    Chart.Bar = Chart.Base.extend({
        renderer: 'bar',

        parse: function (response) {
            var series = {};
            _.each(response.rows, function (row) {
                var key = row.key[0];
                if (_.isUndefined(series[key]))
                    series[key] = [];
                series[key].push({ x: Date.UTC(row.key[1], row.key[2] - 1) / 1000, y: row.value });
            });

            var palette = new Rickshaw.Color.Palette();
            var data = _.map(series, function (value, key) {
                return {
                    name: key,
                    data: value,
                    color: palette.color()
                };
            });
            Rickshaw.Series.zeroFill(data);
            return data;
        }
    });

    Chart.Line = Chart.Base.extend({
        renderer: 'line',

        parse: function (response) {
            var series = {};
            _.each(response.rows, function (row) {
                var key = row.key[0];
                if (_.isUndefined(series[key]))
                    series[key] = [];
                series[key].push({ x: Date.UTC(row.key[1], row.key[2] - 1) / 1000, y: row.value });
            });

            var palette = new Rickshaw.Color.Palette();
            var data = _.map(series, function (value, key) {
                return {
                    name: key,
                    data: value,
                    color: palette.color()
                };
            });
            Rickshaw.Series.zeroFill(data);
            return data;
        }
    });

    Chart.Histogram = Chart.Base.extend({
        widgetClassName: 'histogram',
        renderer: 'bar',
        rangeSlider: false,
        x_axis: false,

        parse: function (response) {
            var palette = new Rickshaw.Color.Palette();

            var counts = {};
            _.each(response.rows, function (row) {
                counts[row.value] = counts[row.value] || 0;
                counts[row.value]++;
            }, this);

            var data = _.map(counts, function (value, key) {
                return { x: parseInt(key, 10), y: parseInt(value, 10) };
            });
            return [{
                data: data,
                color: palette.color()
            }];
        }
    });

    Chart.Percentile = Chart.Base.extend({
        widgetClassName: 'percentile',
        renderer: 'bar',
        rangeSlider: false,
        x_axis: false,
        divisions: 5,

        parse: function (response) {
            var palette = new Rickshaw.Color.Palette();

            var amounts = [],
                length = this.divisions,
                divisor = length / response.rows.length;
            while (length--)
                amounts.push(0);

            response.rows.sort(this.sortBy);
            _.each(response.rows, function (row, index) {
                amounts[Math.floor(index * divisor, 10)] += row.value;
            }, this);

            var data = _.map(amounts, function (value, key) {
                return { x: parseInt(key, 10) + 1, y: parseInt(value * divisor, 10) };
            });
            return [{
                data: data,
                color: palette.color()
            }];
        },

        sortBy: function (a, b) {
            return a.value - b.value;
        }
    });

    Chart.Area = Chart.Base.extend({
        widgetClassName: 'area',
        renderer: 'area',
        stroke: true,

        parse: function (response) {
            var series = {};
            _.each(response.rows, function (row) {
                var key = row.key[0];
                if (_.isUndefined(series[key]))
                    series[key] = [];
                series[key].push({ x: Date.UTC(row.key[1], row.key[2] - 1) / 1000, y: row.value });
            });

            var palette = new Rickshaw.Color.Palette();
            var data = _.map(series, function (value, key) {
                return {
                    name: key,
                    data: value,
                    color: palette.color()
                };
            });
            Rickshaw.Series.zeroFill(data);
            return data;
        },

        renderHoverDetail: function () {
            if (this.hoverDetail === false)
                return;
            var self = this;
            new Rickshaw.Graph.HoverDetail({
                graph: this.graph,
                xFormatter: function (x) {
                    var date = new Date(x * 1000);
                    return _.string.sprintf('%s %d', Chart.MONTHS[date.getUTCMonth()], date.getUTCFullYear());
                },
                formatter: function (series, x, y) {
                    return series.name + ': ' + y + (self.y_label ? ' ' + self.y_label : '');
                }
            });
        }
    });

    return Chart;
});
