define(function (require) {
    'use strict';

    var _ = require('underscore'),
        $ = require('jquery'),
        Backbone = require('backbone_rollup'),
        Widgets = require('widgets/Base'),
        gmaps = require('gmaps'),
        Map = { },
        i18n = require('lib/i18n'),
        gettext = i18n.gettext;

    Map.KmlOverlayControl = Widgets.ItemView.extend({
        tagName: 'div',
        widgetClassName: 'overlay',
        widgetEvents: {
            'change input[type=checkbox]': 'onCheckboxChange'
        },

        initialize: function (options) {
            this._super('initialize', arguments);
            this.map = options.map;
            this.kmls = options.kmls;
        },

        onRender: function () {
            this.$el.append('<a>' + gettext('Data Overlays') + '</a><ul />');
            _.each(this.kmls, function (kml) {
                this.addKml(kml);
            }, this);
        },

        addKml: function (kml) {
            this.$('ul').append('<li><input type="checkbox" /><label>' + kml.name + '</label></li>');
        },

        onCheckboxChange: function (event) {
            var index = $(event.target).parent().index(),
                kml = this.kmls[index];
            if (_.isUndefined(kml.overlay))
                kml.overlay = new gmaps.KmlLayer(kml.url, { preserveViewport: true, suppressInfoWindows: false });
            kml.overlay.setMap(event.target.checked ? this.map : null);
        },

        onClose: function () {
            this._super('onClose', arguments);
            _.each(this.kmls, function (kml) {
                if (kml.overlay)
                    gmaps.event.clearInstanceListeners(kml.overlay);
            });
        }
    });

    Map.MarkerOverlayControl = Widgets.ItemView.extend({
        tagName: 'div',
        widgetClassName: 'overlay',
        widgetEvents: {
            'change input[type=checkbox]': 'onCheckboxChange'
        },

        initialize: function (options) {
            this._super('initialize', arguments);
            this.map = options.map;
            this.types = options.types;
        },

        onRender: function () {
            this.$el.append('<a>' + gettext('Markers') + '</a><ul />');
            _.each(this.types, function (type, name) {
                this.addType(name, type);
            }, this);
        },

        addType: function (name, type) {
            this.$('ul').append(
                _.string.sprintf('<li><input type="checkbox" name="%s" %s /><label>%s</label></li>',
                name, type.visible ? 'checked' : '', type.name)
            );
        },

        isChecked: function (type) {
            return this.$(_.string.sprintf('input[name=%s]', type)).is(':checked');
        },

        onCheckboxChange: function (event) {
            var target = $(event.target),
                name = target.attr('name'),
                type = this.types[name],
                checked = target.is(':checked');
            _.each(this.options.markers, function (marker) {
                if (marker.type === name)
                    marker.marker.setVisible(checked);
            });
        }
    });

    Map.Marker = Widgets.ItemView.extend({
        initialize: function (options) {
            _.bindAll(this, 'onClick');
            this._super('initialize', arguments);
            var location = this.model.get('location'),
                types = this.parent.options.markers,
                overlay = this.parent.markerOverlay;
            this.type = this.model.get('type');
            this.marker = new gmaps.Marker({
                flat: true,
                icon: types[this.type] ? types[this.type].symbol : undefined,
                position: new gmaps.LatLng(location[0], location[1]),
                visible: overlay ? overlay.isChecked(this.type) : true
            });
        },

        onRender: function () {
            this._super('onRender', arguments);
            this.marker.setMap(this.parent.map);
            gmaps.event.addListener(this.marker, 'click', this.onClick);
        },

        onClick: function (event) {
            var infowin = this.parent.infowin,
                content = this.parent.options.markers[this.type].infowinContent;
            infowin.setContent('<div class="info-window">' + _.string.sprintf(content, this.model.toJSON()) + '</div>');
            infowin.open(this.parent.map, this.marker);
        },

        onClose: function () {
            this._super('onClose', arguments);
            this.marker.setMap(null);
            gmaps.event.clearInstanceListeners(this.marker);
            delete this.marker;
        }
    });

    Map.Base = Widgets.CompositeView.extend({
        tagName: 'div',
        widgetClassName: 'map',
        itemView: Map.Marker,

        initialize: function (options) {
            _.bindAll(this, 'onBoundsChanged');
            this._super('initialize', arguments);
            // store options for data fetching
            this.options = options;
            this.collection.urlOptions = this._getUrlOptions();
            window.testmap = this;
        },

        _getUrlOptions: function () {
            return {
                view: 'location',
                limit: 500
            };
        },

        // google maps don't work by appending html like this
        appendHtml: function (collectionView, itemView, index) { },

        onShow: function () {
            this._super('onShow', arguments);
            // create base map
            this.$el.css({
                height: this.$el.parent().height() || '100%',
                width: this.$el.parent().width() || '100%'
            });
            this.map = new gmaps.Map(this.el, this.options || {});
            gmaps.event.addListener(this.map, 'zoom_changed', this.onBoundsChanged);
            gmaps.event.addListener(this.map, 'dragend', this.onBoundsChanged);
            // add overlays
            this.addInfoWindow();
            this.addKmlOverlay();
            this.addMarkerOverlay();
            // grab the marker data
            // no need to fetch asynchrously, since the "empty" view is still a valid map
            this.collection.fetch();
        },

        onClose: function () {
            if (this.infowin) {
                this.infowin.close();
                delete this.infowin;
            }
            if (this.kmlOverlay) {
                this.kmlOverlay.close();
                delete this.kmlOverlay;
            }
            if (this.markerOverlay) {
                this.markerOverlay.close();
                delete this.markerOverlay;
            }
            if (this.map) {
                gmaps.event.clearInstanceListeners(this.map);
                delete this.map;
            }
            this._super('onClose', arguments);
        },

        onBoundsChanged: function () {
            var bounds = this.map.getBounds(),
                ne = bounds.getNorthEast(),
                sw = bounds.getSouthWest();
            this.collection.urlOptions.startkey = [sw.lat(), sw.lng()];
            this.collection.urlOptions.endkey = [ne.lat(), ne.lng()];
            this.collection.fetch();
        },

        addInfoWindow: function () {
            this.infowin = new gmaps.InfoWindow();
        },

        addKmlOverlay: function () {
            if (_.isArray(this.options.kmls)) {
                this.kmlOverlay = new Map.KmlOverlayControl({
                    map: this.map,
                    kmls: this.options.kmls
                });
                this.kmlOverlay.render();
                this.map.controls[gmaps.ControlPosition.TOP_RIGHT].push(this.kmlOverlay.el);
            }
        },

        addMarkerOverlay: function () {
            if (_.isObject(this.options.markers)) {
                this.markerOverlay = new Map.MarkerOverlayControl({
                    markers: this.children,
                    types: this.options.markers
                });
                this.markerOverlay.render();
                this.map.controls[gmaps.ControlPosition.TOP_RIGHT].push(this.markerOverlay.el);
            }
        }
    });

    return Map;
});
