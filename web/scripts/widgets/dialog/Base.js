﻿define(function (require) {
    'use strict';

    var _ = require('underscore'),
        $ = require('jquery'),
        Backbone = require('backbone_rollup'),
        Widgets = require('widgets/Base'),
        Mixins = require('widgets/Mixins'),
        Dialog = { },
        i18n = require('lib/i18n'),
        gettext = i18n.gettext;

    Dialog.Base = Widgets.Layout.mixin(Mixins.Positionable, Mixins.Visible, {
        widgetClassName: 'dialog',
        widgetEvents: {
            'click .close': 'onCloseClick'
        },
        regions: {
            contentRegion: 'section.content'
        },

        initialize: function (options) {
            this.options = options || {};
            this._super('initialize', arguments);
            this.container = $(options.container || 'body');
            _.bindAll(this, 'reposition');
            $(window).on('resize', _.throttle(this.reposition, 100));
        },

        onRender: function () {
            this._super('onRender', arguments);
            this.hide();
            this.$el.contents().wrapAll('<div class="wrapper" />').wrapAll('<section class="content" />');
            if (this.options.close !== false)
                this.$el.prepend('<span class="close" />');
            if (this.options.modal === true)
                this.$el.prepend('<div class="modal_mask" />');
            this.container.append(this.$el);
            this.position(this.container, this.options.alignment || Mixins.Alignments.CENTER);
        },

        onCloseClick: function (event) {
            this.disappear();
            event.stopPropagation();
        },

        onClose: function () {
            this._super('onClose', arguments);
            this.$el.find('.close').off();
        }
    });

    return Dialog;
});
