﻿define(function (require) {
    'use strict';

    var _ = require('underscore'),
        $ = require('jquery'),
        Backbone = require('backbone_rollup'),
        Window = require('widgets/dialog/Window'),
        i18n = require('lib/i18n'),
        gettext = i18n.gettext;

    // The Confirm dialog creates a modal dialog that presents the user with a set of buttons.
    var Confirm = Window.Base.extend({
        widgetClassName: 'confirm',

        onButtonClick: function () {
            this._super('onButtonClick', arguments);
            // automatically hide this control
            this.disappear();
            // clean up
            this.close();
        }
    });

    // public interface for the Confirm control is simply a function that accepts text and a callback.
    // the callback parameter can be a callback, a trigger object, or an array of buttons. Valid calls are:
    //    var Confirm = require('widgets/dialog/Confirm');
    //    Confirm.show('My Message', my.callback);
    //    Confirm.show('My Message', { vent: me, name: 'me:trigger' });
    //    Confirm.show('My Message', [{text: 'Button 1'}, {text: 'Button 2'}, {text: 'Button 3'}] );
    return {
        show: function (text, callback) {
            var buttons = _.isArray(callback) ? callback : [{
                text: gettext('Yes'),
                callback: _.isFunction(callback) ? callback : undefined,
                trigger: _.isFunction(callback) ? undefined : callback
            }, {
                text: gettext('No')
            }];

            var c = new Confirm({
                close: false,
                modal: true,
                template: text,
                buttons: buttons
            });
            c.render();
            c.appear();
        }
    };
});