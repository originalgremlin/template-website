﻿define(function (require) {
    'use strict';

    var _ = require('underscore'),
        $ = require('jquery'),
        Backbone = require('backbone_rollup'),
        Dialog = require('widgets/dialog/Base'),
        Window = { },
        i18n = require('lib/i18n'),
        gettext = i18n.gettext;

    Window.Base = Dialog.Base.extend({
        widgetClassName: 'window',
        widgetEvents: {
            'click section.buttons button': 'onButtonClick'
        },
        regions: {
            contentRegion: 'section.content',
            buttonsRegion: 'section.buttons'
        },

        initialize: function () {
            this._super('initialize', arguments);
            this.buttons = this.buttons || [];
        },

        onRender: function () {
            this._super('onRender', arguments);
            this.$el.find('div.wrapper').append('<section class="buttons" />');
            _.each(this.options.buttons, function (button) {
                this.addButton(button);
            }, this);
        },

        addButton: function (button) {
            this.buttons.push(button);
            this.$el.find('section.buttons').append($('<button />').html(button.text));
            this.reposition();
        },

        onButtonClick: function (event) {
            var target = $(event.currentTarget),
                index = target.index(),
                callback = this.buttons[index].callback,
                triggerOption = this.buttons[index].trigger;
            if (callback)
                callback();
            if (triggerOption && triggerOption.vent && triggerOption.name)
                triggerOption.vent.trigger(triggerOption.name, triggerOption.data);
            event.stopPropagation();
        },

        onClose: function () {
            this._super('onClose', arguments);
            _.each(this.$el.find('section.buttons button'), function (button) {
                $(button).off();
            }, this);
        }
    });

    return Window;
});