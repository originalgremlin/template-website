﻿define(function (require) {
    'use strict';

    var _ = require('underscore'),
        Backbone = require('backbone_rollup'),
        Widgets = require('widgets/Base'),
        Tree = { },
        i18n = require('lib/i18n'),
        gettext = i18n.gettext;

    Tree.Node = Widgets.CompositeView.extend({
        template: '<a>{{ %s }}</a><ul></ul>',
        tagName: 'li',
        widgetClassName: 'node',
        widgetEvents: {
            'click .node.branch > a': 'selectBranch',
            'click .node.leaf > a': 'selectLeaf'
        },

        initialize: function (options) {
            _.extend(this.options, this.parent.options, {
                selected: false,
                nameAttr: this.parent.options.nameAttr || 'name',
                itemsAttr: this.parent.options.itemsAttr || 'items'
            });
            this.selected = this.options.selected;
            this.template = _.string.sprintf(this.template, this.options.nameAttr);
            this.collection = new this.options.collectionClass(this.model.get(this.options.itemsAttr));
        },

        appendHtml: function (collectionView, itemView) {
            collectionView.$("ul:first").append(itemView.el);
        },

        onRender: function () {
            if (this.isLeaf()) {
                this.$el.addClass('leaf');
                this.$("ul:first").remove();
            } else {
                this.$el.addClass('branch');
                this.$("ul:first").hide();
            }
        },

        isLeaf: function () {
            return !(this.collection && this.collection.length > 0);
        },

        select: function () {
            if (!this.selected) {
                this.selected = true;
                this.$el.addClass('selected');
                this.$("ul:first").slideDown();
                this.deselectChildren();
                this.deselectSiblings();
                // publish selection of the most specific category selection
                this.trigger(this.isLeaf() ? 'tree:select:leaf' : 'tree:select:branch', this, this.model);
                // if there is only one child, select that one to save the user one click
                if (this.children && _.values(this.children).length === 1)
                    this.selectFirstChild();
            }
        },

        deselect: function () {
            if (this.selected) {
                this.selected = false;
                this.$el.removeClass('selected');
                this.$("ul:first").slideUp();
                this.deselectChildren();
                // publish selection of the most specific category deselection
                this.trigger(this.isLeaf() ? 'tree:deselect:leaf' : 'tree:deselect:branch', this, this.model);
            }
        },

        selectBranch: function (event) {
            event.stopPropagation();
            event.preventDefault();
            this[this.selected ? 'deselect' : 'select']();
        },

        selectLeaf: function (event) {
            event.stopPropagation();
            event.preventDefault();
            this.select();
        },

        // we only allow one node to be selected at any level
        deselectSiblings: function () {
            var siblings = _.without(this.parent.children, this);
            _.invoke(siblings, 'deselect');
        },

        deselectChildren: function () {
            var children = _.values(this.children);
            _.invoke(children, 'deselect');
        },

        // select the first child of this node, if one exists
        selectFirstChild: function () {
            if (this.children)
                _.first(_.values(this.children)).select();
        }
    });

    Tree.Base = Widgets.CollectionView.extend({
        widgetClassName: 'tree',
        itemView: Tree.Node,
        itemViewContainer: 'ul',
        tagName: 'ul',

        initialize: function (options) {
            this._super('initialize', arguments);
            _.extend(this.options, {
                collectionClass: Backbone.CachedCollection,
                fetchErrorMessage: gettext('There was an error retreiving your items.')
            });
            this.collection = new this.options.collectionClass();
        },

        onRender: function () {
            this.selectOnlyChild();
        },

        fetch: function () {
            if (!this.collection) return;
            var self = this;
            self.$el.addClass('loading');
            this.collection.fetch({
                success: function () {
                    self.$el.removeClass('loading');
                    self.setSelected(self.options.selected);
                },
                error: function () {
                    self.$el.removeClass('loading').addClass('error').text(self.options.fetchErrorMessage);
                }
            });
        },

        getSelected: function () {
            var rv = [],
                callback = function (child) { return child.selected; },
                selected = _.find(this.children, callback);
            while (selected) {
                rv.push(selected.model.get('name'));
                selected = _.find(selected.children, callback);
            }
            return rv;
        },

        setSelected: function (names) {
            var name = names.shift(),
                callback = function (child) { return child.model.get('name') === name; },
                selected = this;
            while (name && selected) {
                selected = _.find(selected.children, callback);
                if (selected)
                    selected.select();
                name = names.shift();
            }
        },

        // select only the first child of this node, if one exists
        selectOnlyChild: function () {
            if (this.children && _.size(this.children) === 1)
                _.first(_.values(this.children)).select();
        },

        // select the first child of this node, if one exists
        selectFirstChild: function () {
            if (this.children && _.size(this.children) > 0)
                _.first(_.values(this.children)).select();
        }
    });

    return Tree;
});
