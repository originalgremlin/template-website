/*
    var a = new Widgets.Accordion.Base({
        el: '#content',
    });
    a.render();
*/

define(function (require) {
    'use strict';

    var _ = require('underscore'),
        $ = require('jquery'),
        Backbone = require('backbone_rollup'),
        Widgets = require('widgets/Base'),
        Drawer = { };

    Drawer.Base = Widgets.ItemView.extend({
        widgetClassName: 'drawer',
        widgetEvents: {
            'click .handle': 'onHandleClick'
        },

        render: function () {
            this.$el.append('<div class="handle" />');
            this.$el.addClass(this.className());
            return this;
        },

        onHandleClick: function (event) {
            this.$el.toggleClass('closed');
        }
    });

    return Drawer;
});
