// https://github.com/jrburke/r.js/blob/master/build/example.build.js
//
// To build using the closure compiler from project root:
// java -classpath scripts/build/js.jar:scripts/build/compiler.jar org.mozilla.javascript.tools.shell.Main /usr/local/bin/r.js -o scripts/build.js

({
    closure: {
        CompilerOptions: { },
        CompilationLevel: 'SIMPLE_OPTIMIZATIONS',
        loggingLevel: 'WARNING'
    },
    inlineText: true,
    logLevel: 2,
    name: 'main',
    optimize: 'closure',
    out: '../static/scripts/main.js',
    paths: {
        async: 'lib/requirejs-plugins/async',
        backbone: 'lib/backbone.custom',
        backbone_rollup: 'lib/backbone.rollup',
        gmaps: 'lib/requirejs-plugins/gmaps',
        i18n: 'lib/requirejs-plugins/i18n',
        jquery: 'lib/jquery',
        text: 'lib/requirejs-plugins/text',
        underscore: 'lib/underscore.custom'
    },
    preserveLicenseComments: false,
    shim: {
        'lib/backbone': { deps: ['underscore', 'jquery'], exports: 'Backbone' },
        'lib/backbone.collectionbinder': { deps: ['backbone', 'lib/backbone.modelbinder'] },
        'lib/backbone.validation': { deps: ['backbone'] },
        'lib/d3.v2': { exports: 'd3' },
        'lib/jquery.ui': { deps: ['jquery'] },
        'lib/json2': { exports: 'JSON' },
        'lib/rickshaw': { deps: ['lib/d3.v2'], exports: 'Rickshaw' },
        'lib/underscore': { exports: '_' },
        'lib/underscore.custom': { deps: ['lib/underscore'], exports: '_' },
        'lib/uuid': { exports: 'uuid' }
    },
    useStrict: true,
    wrap: true
})
