define(function (require) {
    'use strict';

    var _ = require('underscore'),
        $ = require('jquery'),
		i18n = require('lib/i18n'),
        Backbone = require('backbone_rollup'),
        Models = require('modules/main/models'),
		Views = { };

    Views.ItemView = Backbone.Marionette.ItemView.extend({
        onBeforeRender: function () {
            this.template = i18n.parse(this.template);
        }
    });

    Views.Example = Views.ItemView.extend({
        template: require('text!templates/main/example.html')
    });

	return Views;
});
