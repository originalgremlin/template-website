define(function (require) {
    'use strict';

    var Modules = { };

    Modules.Main = {
        Models: require('modules/main/models'),
        Views: require('modules/main/views')
    };

    return Modules;
});
