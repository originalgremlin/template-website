define(function (require) {
    'use strict';

    var _ = require('underscore'),
		$ = require('jquery'),
		Backbone = require('backbone_rollup'),
		Modules = require('modules/all');

	var Application = new Backbone.Marionette.Application();

	// pre-initialization
	Application.addRegions({
		region_header: 'body > header',
		region_content: 'body > section.content'
	});

	Application.on('initialize:before', function (options) {
		$('body').addClass('initializing');
	});

	Application.addInitializer(function (options) {
		$('body').ajaxStart(function () {
			$(this).addClass('loading');
		});
		$('body').ajaxStop(function () {
			$(this).removeClass('loading');
		});
	});

	// post-initialization
	Application.on('initialize:after', function (options) {
		$('body').removeClass('initializing');
	});

	$(document).ready(function (event) {
		$('body').removeClass('requiring');
		Application.start();
	});

	return Application;
});
