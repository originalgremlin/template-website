define(function (require) {
    'use strict';

    var Backbone = require('backbone_rollup'),
        Main = require('modules/main/models'),
        Models = { };

    Models.Model = Main.Model.extend({
        defaults: {
            type: '<% name %>'
        }
    }, {
        converters: {
        }
    });

    Models.Collection = Main.Collection.extend({
        model: Models.Model
    });

    return Models;
});
