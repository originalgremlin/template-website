define(function (require) {
    'use strict';

    var _ = require('underscore'),
        i18n = require('lib/i18n'),
        Main = require('modules/main/views'),
        Models = require('modules/<% name %>/models'),
        Views = { };

    Views.Example = Main.ItemView.extend({
    });

    return Views;
});
