    Modules.<% Name %> = {
        Models: require('modules/<% name %>/models'),
        Views: require('modules/<% name %>/views')
    };

